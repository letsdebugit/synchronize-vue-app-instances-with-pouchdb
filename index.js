import { connect, loadMessages, saveMessage } from './database.js'

const App = {
  data () {
    return {
      // Logged-in user
      nickname: '',
      isLoggedIn: false,
      // Chat history
      messages: [],
      // Entered message
      message: ''
    }
  },

  methods: {
    // Load messages from database into chat history
    async load () {
      this.messages = await loadMessages()
    },

    // Logs in a user
    login () {
      this.nickname = this.nickname.trim()
      this.isLoggedIn = Boolean(this.nickname)
    },

    // Logs out the user
    logout () {
      this.nickname = ''
      this.isLoggedIn = false
    },

    // Send the entered message
    send () {
      saveMessage(this.nickname, this.message)
      this.message = ''
    },

    // Renders user-friendly time from message timestamp
    timeOf (message) {
      return new Date(message.timestamp).toTimeString().substr(0, 8)
    }
  },

  created () {
    connect({
      onConnected: () => this.load(),
      onChanged: () => this.load(),
      onError: error => console.error(error)
    })
  }
}

Vue
  .createApp(App)
  .mount('body')
